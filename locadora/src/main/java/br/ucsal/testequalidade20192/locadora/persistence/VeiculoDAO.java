package br.ucsal.testequalidade20192.locadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.exception.VeiculoNaoEncontradoException;

public class VeiculoDAO {

	private static List<Veiculo> veiculos = new ArrayList<>();

	public static Veiculo obterPorPlaca(String placa) throws VeiculoNaoEncontradoException {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca().equalsIgnoreCase(placa)) {
				return veiculo;
			}
		}
		throw new VeiculoNaoEncontradoException();
	}
	
	public static void insert(Veiculo veiculo){
		veiculos.add(veiculo);
	}

}
