package br.ucsal.testequalidade20192.builder;

import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	public static final String DEFAULT_PLACA = "123-ABC";
    public static final Integer DEFAULT_ANO = 2000;
    public static final Double DEFAULT_VALOR = 500.00;
 
    private String placa = DEFAULT_PLACA;

	private Integer ano = DEFAULT_ANO;

	private Modelo modelo;

	private Double valorDiaria = DEFAULT_VALOR;

	private SituacaoVeiculoEnum situacao = SituacaoVeiculoEnum.DISPONIVEL;
 
    private VeiculoBuilder() {
    }
 
    public static VeiculoBuilder aVeiculo() {
        return new VeiculoBuilder();
    }
 
    public VeiculoBuilder withModelo(Modelo modelo) {
        this.modelo = modelo;
        return this;
    }
 
    public VeiculoBuilder withPlaca(String placa) {
        this.placa = placa;
        return this;
    }
 
    public VeiculoBuilder withAno(Integer ano) {
        this.ano = ano;
        return this;
    }
 
    public VeiculoBuilder withValorDiaria(Double valor) {
        this.valorDiaria = valor;
        return this;
    }
 
    public VeiculoBuilder withSituacao(SituacaoVeiculoEnum situacao){
        this.situacao = situacao;
        return this;
    }
 
    public VeiculoBuilder withNoSituacao() {
        this.situacao = null;
        return this;
    }
 
    public VeiculoBuilder withNoAno() {
        this.ano = 0;
        return this;
    }
    
    public VeiculoBuilder withNoModelo() {
        this.modelo = null;
        return this;
    }
    public VeiculoBuilder withNoValor() {
        this.valorDiaria = 0.00;
        return this;
    }
    public VeiculoBuilder withNoPlaca() {
        this.placa = "";
        return this;
    }
    public Veiculo build() {
        Veiculo veiculo = new Veiculo(placa,ano,modelo,valorDiaria);
        veiculo.setSituacao(situacao);
        return veiculo;
    }

}
