package br.ucsal.testequalidade20192.builder;

import br.ucsal.testequalidade20192.locadora.dominio.Cliente;

public class ClienteBuilder {
	 	private String cpf = "1";

		private String nome = "Bruna";

		private String telefone = "98181818";
		
	private ClienteBuilder() {
    }
 
    public static ClienteBuilder aCliente() {
        return new ClienteBuilder();
    }
 
    public ClienteBuilder withCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }
 
    public ClienteBuilder withPlaca(String nome) {
        this.nome = nome;
        return this;
    }
 
    public ClienteBuilder withAno(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public Cliente build() {
        Cliente Cliente = new Cliente(cpf,nome,telefone);
        return Cliente;
    }
}
