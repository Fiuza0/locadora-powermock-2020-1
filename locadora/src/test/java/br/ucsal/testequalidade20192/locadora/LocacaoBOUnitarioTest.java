package br.ucsal.testequalidade20192.locadora;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.testequalidade20192.builder.ClienteBuilder;
import br.ucsal.testequalidade20192.builder.VeiculoBuilder;
import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;
@RunWith(PowerMockRunner.class)
@PrepareForTest({ LocacaoBO.class })
public class LocacaoBOUnitarioTest {

	/**
	 * Verificar se ao locar um veículo disponível para um cliente cadastrado, um
	 * contrato de locação é inserido.
	 * 
	 * Método:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observação1: lembre-se de mocar os métodos necessários nas objetos
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observação2: lembre-se de que o método locarVeiculos é um método command.
	 * 
	 * @throws Exception
	 */
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		LocacaoBO objetoLocacao = new LocacaoBO();
		LocacaoBO objetoLocacaoSpy = PowerMockito.spy(objetoLocacao);
		ClienteDAO objetoClienteDAO = new ClienteDAO();
		ClienteDAO objetoClienteDAOSpy = PowerMockito.spy(objetoClienteDAO);
		VeiculoDAO objetoVeiculoDAO = new VeiculoDAO();
		VeiculoDAO objetoVeiculoDAOSpy = PowerMockito.spy(objetoVeiculoDAO);
		
		PowerMockito.mockStatic(LocacaoBO.class);
		
		ClienteBuilder cliente = ClienteBuilder.aCliente();
		cliente.withCpf("1");
		PowerMockito.when(objetoClienteDAOSpy.obterPorCpf("1")).thenReturn(cliente.build());
		
		VeiculoBuilder veiculo = VeiculoBuilder.aVeiculo();
		veiculo.withAno(2019).withValorDiaria(200.00)
		.withSituacao(SituacaoVeiculoEnum.DISPONIVEL).withPlaca("ABC-123");
		PowerMockito.when(objetoVeiculoDAOSpy.obterPorPlaca("ABC-123")).thenReturn(veiculo.build());
		PowerMockito.when(objetoLocacaoSpy,"locarVeiculos").thenReturn(30);

		List<String> placas = new ArrayList<String>();
		placas.add(veiculo.build().getPlaca());
		Date data = new Date();
		objetoLocacaoSpy.locarVeiculos("1", placas, data, 1);
		
		
		PowerMockito.verifyPrivate(ClienteDAO.class).invoke("obterPorCpf");
		PowerMockito.verifyPrivate(VeiculoDAO.class).invoke("obterPorPlaca");
		LocacaoBO.locarVeiculos("1", placas, data, 1);
		PowerMockito.verifyNoMoreInteractions(LocacaoBO.class);
		
	}
}
